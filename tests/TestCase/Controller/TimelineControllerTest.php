<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Controller\TimelineController;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;

/**
 * App\Controller\TimelineController Test Case
 *
 * @uses \App\Controller\TimelineController
 */
class TimelineControllerTest extends TestCase
{
    use IntegrationTestTrait;
}
