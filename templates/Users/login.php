 <div class="main-content">
    <!-- Header -->
    <div class="header bg-gradient-primary py-7 py-lg-8 pt-lg-9">
      <div class="container">
        <div class="header-body text-center mb-7">
          <div class="row justify-content-center">
            <div class="col-xl-5 col-lg-6 col-md-8 px-5">
              <h1 class="text-white">Welcome!</h1>
              <p class="text-lead text-white">to Microblog</p>
            </div>
          </div>
        </div>
      </div>
      <div class="separator separator-bottom separator-skew zindex-100">
        <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
          <polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>
        </svg>
      </div>
    </div>
    <!-- Page content -->
    <div class="container mt--8 pb-5">
      <div class="row justify-content-center">
        <div class="col-lg-5 col-md-7">
          <div class="card bg-secondary border-0 mb-0">
            <div class="card-header bg-transparent pb-5">
              <div class="text-muted text-center mt-2"><small>Sign in</small></div>
              <div class="btn-wrapper text-center">
                <img src="https://www.microlog.it/wp-content/uploads/2019/05/microblog-verde-01.png" style="max-width: 50vh; max-height: 10vh">
               
              </div>
            </div>
            <div class="card-body px-lg-5 py-lg-5 mt--5">
              <div class="text-muted text-center mb-2"><?= $this->Flash->render() ?></div>
              <?= $this->Form->create($user) ?>
                <?= $this->Form->control('email',['label' => false, 'class' => 'form-control , mb-3','placeholder'=> 'Email']);?>
                <?= $this->Form->control('password',['label' => false, 'type' => 'password' , 'class' => 'form-control , mb-3','placeholder'=> 'Password']);?>
             
                <div class="text-center">
                  <button type="submit" class="btn btn-primary my-4">Sign in</button>
                </div>
              <?= $this->Form->end() ?>
              <div class="text-center ">
                <a href="register">Create an account</a>
            </div>
            </div>

          </div>
          <div class="row mt-3">
           <!--  <div class="col-6">
              <a href="#" class="text-light"><small>Forgot password?</small></a>
            </div> -->
           <!--  <div class="col-6 text-right">
              <a href="#" class="text-light"><small>Create new account</small></a>
            </div> -->
          </div>
        </div>
      </div>
    </div>
  </div>